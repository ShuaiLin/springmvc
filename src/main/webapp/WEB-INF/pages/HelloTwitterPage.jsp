<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
	<title>twitter test</title>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			drawTable(${data});
	    });
		
		function drawTable(data) {
		    for (var i = 0; i < data.length; i++) {
		        drawRow(data[i]);
		    }
		}

		function drawRow(rowData) {
		    var row = $("<tr />")
		    $("#homeTimelineDataTable").append(row);
		    row.append($("<td rowspan='2'><img src='" + rowData.user.profile_image_url + "' /></td>"));
		    row.append($("<td style='border-bottom: 0px;'><b>" + rowData.user.name + "</b> @" + rowData.user.screen_name + "<br />" + rowData.text +"</td>"));
		    row = $("<tr />")
		    $("#homeTimelineDataTable").append(row);
		    row.append($("<td>Retweet: " + rowData.retweet_count + "&nbsp;&nbsp;&nbsp;Favorite: " + rowData.favorite_count + "</td>"));
		}
	</script>
	
	<style>
		body {
		    background-color: #CCFFCC;
		}
		table {
			background-color: white;
		    width: 640px;
		    margin: 0 auto;
		    font-size: 18px;
		}
		
		table td {
			padding: 10px;
			border-bottom: 1px solid black;
		}
	</style>
</head>
<body>
	<h1>xxx Twitter</h1>

	<table id="homeTimelineDataTable">
	</table>

</body>
</html>