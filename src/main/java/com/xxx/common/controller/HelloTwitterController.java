package com.xxx.common.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.basic.DefaultOAuthConsumer;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

public class HelloTwitterController extends AbstractController {
	
	private static final String TWITTER_CONSUMER_KEY = "your consumer key";
	private static final String TWITTER_SECRET_KEY = "your secret key";
	private static final String TWITTER_ACCESS_TOKEN = "your access token";
	private static final String TWITTER_ACCESS_TOKEN_SECRET = "your access token secret";

	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		// fetch numbers of HomeTimeline, max: 200
		int count = 20;
		
		// TWITTER_KEY
	    OAuthConsumer consumer = new DefaultOAuthConsumer(TWITTER_CONSUMER_KEY, TWITTER_SECRET_KEY);
	 
	    // TWITTER_ACCESS_TOKEN
	    consumer.setTokenWithSecret(TWITTER_ACCESS_TOKEN, TWITTER_ACCESS_TOKEN_SECRET);

	    // set request information
	    URL url = new URL("https://api.twitter.com/1.1/statuses/home_timeline.json?count=" + count);
	    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	    connection.setRequestMethod("GET");
	    consumer.sign(connection);
	 
	    // get home_timeline json data
	    BufferedReader br = new BufferedReader(new InputStreamReader(
	            connection.getResponseCode() == HttpURLConnection.HTTP_OK ? connection.getInputStream() : connection.getErrorStream(), "UTF-8")
	    );
	    String line = null;
	    String json = "";
	    while ((line = br.readLine()) != null) {
	    	System.out.println(line);
	    	json = line;
	    }

		ModelAndView model = new ModelAndView("HelloTwitterPage");
		model.addObject("data", json);

		return model;
	}

}